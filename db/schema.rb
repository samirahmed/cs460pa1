# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130313190945) do

  create_table "albums", :force => true do |t|
    t.string  "name",   :limit => 50, :null => false
    t.integer "userid"
    t.date    "date",                 :null => false
  end

  create_table "comments", :force => true do |t|
    t.date    "date",                   :null => false
    t.string  "text",    :limit => 500, :null => false
    t.integer "photoid"
    t.integer "userid"
  end

  create_table "friends", :id => false, :force => true do |t|
    t.integer "user1id", :null => false
    t.integer "user2id", :null => false
  end

  create_table "likes", :id => false, :force => true do |t|
    t.integer "userid",  :null => false
    t.integer "photoid", :null => false
  end

  create_table "photos", :force => true do |t|
    t.string  "image",   :limit => 500, :null => false
    t.string  "caption", :limit => 500
    t.integer "albumid"
  end

  create_table "tagged", :id => false, :force => true do |t|
    t.integer "photo_id",               :null => false
    t.string  "tag_id",   :limit => 20, :null => false
  end

  create_table "tags", :id => false, :force => true do |t|
    t.string "word", :limit => 20, :null => false
  end

  create_table "users", :force => true do |t|
    t.string "email",          :limit => 50,  :null => false
    t.date   "dob",                           :null => false
    t.string "password",       :limit => 30,  :null => false
    t.string "first_name",     :limit => 20,  :null => false
    t.string "last_name",      :limit => 20,  :null => false
    t.string "gender",         :limit => 20
    t.string "currentcity",    :limit => 50
    t.string "currentstate",   :limit => 20
    t.string "currentcountry", :limit => 20
    t.string "homecity",       :limit => 50
    t.string "homestate",      :limit => 20
    t.string "homecountry",    :limit => 20
    t.string "education",      :limit => 100
  end

  add_index "users", ["email"], :name => "users_email_key", :unique => true

end
