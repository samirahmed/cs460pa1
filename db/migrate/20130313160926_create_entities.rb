class CreateEntities < ActiveRecord::Migration
  def up

    execute <<-SQL
      CREATE TABLE users(
        id              serial PRIMARY KEY,
        email           VARCHAR(50) UNIQUE NOT NULL,
        dob             DATE  NOT NULL,
        password        VARCHAR(30) NOT NULL,
        first_name      VARCHAR(20) NOT NULL CHECK(first_name <> ''),
        last_name       VARCHAR(20) NOT NULL CHECK(last_name <> ''),
        gender          VARCHAR(20),
        currentcity     VARCHAR(50),
        currentstate    VARCHAR(20),
        currentcountry  VARCHAR(20),
        homecity        VARCHAR(50),
        homestate       VARCHAR(20),
        homecountry     VARCHAR(20),
        education       VARCHAR(100)
      );
    SQL

    execute <<-SQL
      CREATE TABLE albums(  
      id      serial PRIMARY KEY,
      name    varchar(50) NOT NULL CHECK(name<>''),
      userid  integer,
      date    date    NOT NULL,
      FOREIGN KEY (userid) REFERENCES users(id)
      );
    SQL
    
    execute <<-SQL
      CREATE TABLE photos(  
      id      serial PRIMARY KEY,
      image   varchar(500) NOT NULL,
      caption varchar(500),
      albumid integer,
      FOREIGN KEY (albumid) REFERENCES albums(id) ON DELETE CASCADE
      );
    SQL

     execute <<-SQL
      CREATE TABLE tags(
        word VARCHAR(20) NOT NULL CHECK(word<>''),
        PRIMARY KEY(word)
      );
     SQL
  end

  def down
    execute <<-SQL
    DROP TABLE users, albums, photos, tags;
    SQL
  end
end
