class CreateRelations < ActiveRecord::Migration
  def up
    execute <<-SQL
       CREATE TABLE friends(
          user1id integer REFERENCES users(id),
          user2id integer REFERENCES users(id),
          PRIMARY KEY (user1id,user2id),
          CONSTRAINT not_self CHECK ( user1id <> user2id )
       );
    SQL

    execute <<-SQL
      CREATE TABLE tagged(
        photo_id   integer     REFERENCES photos(id) ON DELETE CASCADE,
        tag_id    varchar(20) REFERENCES tags(word) ON DELETE CASCADE,
        PRIMARY KEY (photo_id, tag_id)
      );
    SQL

    execute <<-SQL
      CREATE TABLE comments(
        id        serial        PRIMARY KEY,
        date      date          NOT NULL,
        text      varchar(500)  NOT NULL CHECK(text<>''),
        photoid   integer       REFERENCES photos(id) ON DELETE CASCADE,
        userid    integer       REFERENCES users(id)
      );
    SQL
    
    execute <<-SQL
      CREATE TABLE likes(
        userid    integer   REFERENCES users(id),
        photoid   integer   REFERENCES photos(id) ON DELETE CASCADE,
        PRIMARY KEY (userid,photoid)
      );
    SQL

  end

  def down
    execute <<-SQL
      DROP TABLE likes, comments, tagged, friends;
    SQL
  end
end
