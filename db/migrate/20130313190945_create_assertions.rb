class CreateAssertions < ActiveRecord::Migration
  def up
    execute <<-SQL

CREATE FUNCTION user_cant_comment() RETURNS trigger AS $user_cant_comment$
    BEGIN
        IF EXISTS(  SELECT * 
                    FROM   photos p, albums a
                    WHERE  NEW.photoid = p.id
                      AND  NEW.albumid = a.id
                      AND  a.userid = NEW.userid)
        THEN
          RAISE EXCEPTION 'user cannot comment on own photo';
        END IF;
        RETURN NEW;
    END;
$user_cant_comment$ LANGUAGE plpgsql;

      CREATE TRIGGER owner_cant_comment
        BEFORE UPDATE ON comments
        FOR EACH ROW EXECUTE PROCEDURE user_cant_comment();
    SQL
  end

  def down
  end
end
