# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'factory_girl'
#require_relative '../test/factories/all_factories.rb'

users = 40.times.map{|x| FactoryGirl.create(:user) }
users << FactoryGirl.create(:user, first_name:"samir", last_name:"ahmed", email:"samahmed@bu.edu")

words = Faker::Lorem.words(100)

puts "Creating Random others ... "

User.all.map{ |user| rand(1..3).times.map{|x| FactoryGirl.create(:album, userid:user.id)} }

Album.all.map do |album|
    rand(4..10).times.map{ |x| FactoryGirl.create(:photo, albumid:album.id )}
end

Photo.all.map do |pic|
  rand(1..5).times.map do |x| 
    FactoryGirl.create(:comment ,photoid:pic.id, userid:(User.all-[pic.album.user]).sample.id)
  end
  
  users.sample(rand(1..6)).map do |user|
    Like.find_or_create_by_photoid_and_userid(photoid:pic.id,userid:user.id)
  end

  words.sample(rand(4..6)).map do |word|
    tag = Tag.find_or_create_by_word( FactoryGirl.build(:tag)[:word])
    if tag and !pic.tags.include?(tag)
      pic.tags << tag
    end
  end
end

# build friends
(rand(users.size-10..users.size+10)).times.map do |x|
   pair = users.sample(2)
   pair.first.friend(pair.last.id)
end

User.all.map do |user|
  puts "user: #{user.name}, #{user.email}"
  puts "\talbums: #{user.albums.count}"
  puts "\tphotos: #{user.photos.count}"
  puts "\ttags: #{user.photos.reduce(0){|sum,p| sum+=p.tags.count}}"
  puts "\tlikes: #{user.photos.reduce(0){|sum,p| sum+=p.likes.count}}"
end

