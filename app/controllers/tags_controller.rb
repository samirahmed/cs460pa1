class TagsController < ApplicationController

  before_filter :signed_in_user, only: [:create, :destroy]

  def suggest
    if params[:words] and params[:words]!=""
      @words = params[:words].split(" ")
    else
      @words = Tag.limit(3).map{|item| item.word}
      @example = true
    end
    @suggestions = Tag.suggest(@words) 
  end
  
  def index
    @popular = Tag.popular
  end
  
  def show
    @words = params[:words].split(" ")
    @userid = params[:user]
    
    if @words and @userid.nil?
      @photos = Photo.find_by_wordlist(@words)
    elsif @words and @userid 
      @user = User.from_id(@userid)
      @photos = Photo.find_by_wordlist_and_user_id(@words,@userid)
    end
  end
  
  def create
    @picture = Photo.from_id(params[:photoid])
    ap @picture.get_user
    ap current_user
    if @picture and current_user?(@picture.get_user)
       @user = current_user
       tag = params[:word].downcase[0..19].split(" ").first
       @tag = Tag.find_or_create_by_word(tag)
       if @tag and !@picture.tags.include?(@tag)
          @picture.tags << @tag
          redirect_to :back
       else
          redirect_to :back, :notice => "tag already exists!"
       end
    else
       redirect_to :back, :notice => "error creating tag"
    end
  end

  def destroy
    @tag = Tag.from_word(params[:word])
    if @tag
      @tag.destroy
    end
    redirect_to :back
  end

end
