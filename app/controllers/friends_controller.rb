class FriendsController < ApplicationController

  before_filter :signed_in_user, only: [:create, :destroy]

  def create
    if current_user.friend(params[:id])
      redirect_to :back
    else
      redirect_to :back, :notice => "unable to add friend"
    end
  end

  def destroy
    current_user.unfriend(params[:id])
    redirect_to :back
  end

end
