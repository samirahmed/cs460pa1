class CommentsController < ApplicationController

  before_filter :signed_in_user, only: [:create, :destroy]

  def create
    @picture = Photo.from_id(params[:photoid])
    if @picture.nil? or current_user?(@picture.album.user)
       redirect_to :back, :notice => "you can't comment on your own photo!"
    else
      @user = current_user
      @comment = Comment.new(text:params[:text],userid:current_user.id,photoid:params[:photoid],date:Time.now()) 
      if @comment.save
         redirect_to :back
      else
         flash[:notice] = "error occured"
         redirect_to root_path
      end
    end
  end

  def destroy
    @comment = Comment.from_id(params[:id])
    if @comment and @comment.user == current_user
      @comment.destroy
    end
    redirect_to :back
  end

end
