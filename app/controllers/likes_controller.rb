class LikesController < ApplicationController

  before_filter :signed_in_user, only: [:create, :destroy]

  def create
    @picture = Photo.from_id(params[:photoid])
    puts params.inspect
    if @picture
      @user = current_user
      @like = Like.new(userid:current_user.id,photoid:params[:photoid]) 
      if @like.save
         redirect_to :back
      else
         flash[:notice] = "error occured"
         redirect_to root_path
      end
    else
      puts "error"
      redirect_to :back
    end

  end

  def destroy
    @like = Like.where("photoid = ? AND userid = ?",
                                params[:photoid].to_i,
                                current_user.id).first
    puts @like.inspect
    if @like and @like.userid == current_user.id
      @like.destroy
      redirect_to :back
    else
      redirect_to :back, :notice => "Error Unliking"
    end
  end

end
