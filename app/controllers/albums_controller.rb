class AlbumsController < ApplicationController
  
  before_filter :signed_in_user, only: [:create,:new,:edit, :destroy]

  # GET /albums
  # GET /albums.json
  def index
    @albums = Album.limit(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @albums }
    end
  end

  # GET /albums/1
  # GET /albums/1.json
  def show
    @album = Album.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @album }
    end
  end

  # GET /albums/new
  # GET /albums/new.json
  def new
    @album = Album.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @album }
    end
  end

  # GET /albums/1/edit
  def edit
    @album = Album.from_id(params[:id])
  end

  # POST /albums
  # POST /albums.json
  def create
    @album = Album.new(name:params[:name],date:Time.now(),userid:current_user.id)
    respond_to do |format|
      if @album.save
        
        # add photos
        pics = params[:photo_url].zip(params[:caption]).keep_if{|url,cap| !url.empty? }
        pics.each do |url,cap|
          Photo.create(image:url,caption:cap,albumid:@album.id)
        end
        
        format.html { redirect_to @album, notice: 'Album was successfully created.' }
        format.json { render json: @album, status: :created, location: @album }
      else
        format.html { render action: "new" }
        format.json { render json: @album.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /albums/1
  # PUT /albums/1.json
  def update
    @album = Album.from_id(params[:id]) 
    respond_to do |format|
      @album.name = (params[:name])
      # update pics
      @pictures = @album.photos
      result = ActiveRecord::Base.transaction do
        res = @pictures.zip(params[:photo_url],params[:caption]).map do |pic,url,cap|
          pic.image = url
          pic.caption = cap
          pic.save
        end
        res.all? && @album.save
      end
      if result
        format.html { redirect_to @album, notice: 'Album was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @album.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /albums/1
  # DELETE /albums/1.json
  def destroy
    @album = Album.find(params[:id])
    @album.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end
end
