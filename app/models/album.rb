class Album < ActiveRecord::Base
   attr_accessible :userid, :name, :date
  set_table_name 'albums'
  set_primary_key 'id'

  has_many :photos, :foreign_key => "albumid"
  belongs_to :user, :foreign_key => "userid"

  def cover
    Photo.find_by_sql(["SELECT photos.* FROM photos WHERE photos.albumid =? LIMIT 1",id]).first
  end

  def photos
    Photo.find_by_sql(["SELECT photos.* FROM photos WHERE photos.albumid = ?",id])
  end

  def self.from_id(id)
    Album.find_by_sql([
      "SELECT albums.* FROM albums WHERE albums.id = ? LIMIT 1",id]).first
  end

  def destroy
    Album.connection.execute("DELETE FROM albums WHERE albums.id = ?",id) 
  end
end
