class Like < ActiveRecord::Base
  attr_accessible :photoid, :userid

  belongs_to :photo, :foreign_key => "photoid"
  belongs_to :user, :foreign_key => "userid"

end
