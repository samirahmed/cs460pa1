class Comment < ActiveRecord::Base
  attr_accessible :userid, :photoid, :text, :date 
  belongs_to :user, :foreign_key =>"userid"
  belongs_to :photo, :foreign_key =>"photoid"

  def self.from_id(id)
    Comment.find_by_sql(['
    SELECT comments.*
    FROM comments 
    WHERE comments.id = ?
    LIMIT 1
    ',id]).first

  end
end
