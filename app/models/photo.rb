class Photo < ActiveRecord::Base
  attr_accessible :image, :albumid, :caption
  set_table_name 'photos'
  set_primary_key 'id'

  has_many :likes, :foreign_key => "photoid"
  belongs_to :album, :foreign_key => "albumid"
  has_many :comments, :foreign_key => "photoid"
  has_and_belongs_to_many :tags, :join_table => "tagged"

  def get_comments
    Comment.find_by_sql(["SELECT comments.* FROM comments WHERE comments.photoid = ?",id])
  end

  def get_likes
    Like.find_by_sql(["SELECT likes.* FROM likes WHERE likes.photoid= ?",id])
  end

  def get_user
    User.find_by_sql(["
     SELECT users.*
     FROM photos, users, albums
     WHERE users.id = albums.userid
        AND albums.id = photos.albumid
        AND photos.id = ?", id]).first
  end
  
  def self.from_id(id)
    Photo.find_by_sql([ "
    SELECT photos.*
    FROM photos
    WHERE photos.id = ?
    LIMIT 1
    " ,id]).first
  end

  def self.find_by_wordlist_and_user_id(words, userid)
      word_array = words.map(&:inspect).join(',').gsub("\"","\'")
       (Photo.find_by_sql ["
        SELECT * 
        FROM users, albums, photos 
        WHERE albums.userid = users.id 
          AND photos.albumid = albums.id 
          AND users.id = ? 
          AND EXISTS (SELECT * FROM tagged t WHERE t.tag_id in(#{word_array}))
          AND NOT EXISTS ( 
            ( 
              SELECT t2.tag_id FROM tagged t2
              WHERE t2.tag_id in (#{word_array})
            )
            EXCEPT
            (
              SELECT t3.tag_id FROM tagged t3
              WHERE t3.photo_id = photos.id
            )
           )", userid ]).compact.uniq
  end

  def self.find_by_wordlist(words)
      
      word_array = words.map(&:inspect).join(', ').gsub("\"","\'")
      ( Photo.find_by_sql ["
        SELECT *
        FROM photos p
        WHERE EXISTS( SELECT * FROM tagged t WHERE t.tag_id in (#{word_array}) )
          AND NOT EXISTS(
          (
            SELECT t2.tag_id FROM tagged t2
            WHERE t2.tag_id in (#{word_array})
          )
          EXCEPT
          (
            SELECT t3.tag_id FROM tagged t3
            WHERE t3.photo_id = p.id
          )
         )"]).compact.uniq
  end

end
