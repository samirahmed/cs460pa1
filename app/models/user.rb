class User < ActiveRecord::Base
  set_table_name 'users'
  set_primary_key 'id'

  attr_accessible :password, :first_name, :last_name, :email, :dob, 
                  :homestate, :homecity,:homecountry, 
                  :currentcity, :currentstate, :currentcountry, 
                  :education,:gender

  has_many :albums, :foreign_key => "userid" 
  has_many :photos, :through => :albums , :foreign_key => "userid"
  has_many :likes, :foreign_key => "userid"
  has_many :comments, :foreign_key => "userid"
  #has_and_belongs_to_many :friends, :class_name => 'user', :foreign
  
  
  #attr_accessor :first_name, :last_name, :password, :dob, :email
  
  def get_albums
    Album.find_by_sql([
      "SELECT albums.* 
       FROM albums
       WHERE albums.userid = ?",id])
  end

  def get_photos
    Photo.find_by_sql([
      "SELECT photos.*
       FROM photos INNER JOIN albums ON photos.albumid = albums.id
       WHERE albums.userid= ?", id])
  end

  def self.from_email(email)
    User.find_by_sql([
      "SELECT users.*
       FROM users
       WHERE users.email = ?
       LIMIT 1",email]
    ).first 
  end
  
  def self.from_id(id)
    User.find_by_sql( ["
      SELECT users.* 
      FROM users 
      WHERE users.id = ? 
      LIMIT 1", id]).first
  end

  def friends
    User.find_by_sql([
    "SELECT users.* FROM users
     WHERE users.id in (
       SELECT user2id 
       FROM friends f
       WHERE f.user1id = ?
       UNION
       SELECT user1id
       FROM friends f2
       WHERE f2.user2id = ?
      )",id,id])
  end

  def friend(userid)
    user1id = ActiveRecord::Base::sanitize(userid)
    user2id = id
    User.connection.select_all("
      INSERT INTO friends (user1id,user2id) 
      SELECT #{user1id}, #{user2id} 
      WHERE NOT EXISTS (
        SELECT * FROM friends f
        WHERE (f.user1id = #{user1id} AND f.user2id = #{user2id})
           OR (f.user1id = #{user2id} AND f.user2id = #{user1id}) );
      SELECT user2id, user1id FROM friends f
      WHERE (f.user1id = #{user1id} AND f.user2id= #{user2id}) 
         OR (f.user1id = #{user2id} AND f.user2id= #{user1id})")
  end

  def unfriend(userid)
    user1id = ActiveRecord::Base::sanitize(userid)
    user2id = id
    User.connection.select_all("
      DELETE FROM friends f
      WHERE (f.user1id = #{user1id} AND f.user2id= #{user2id}) 
         OR (f.user1id = #{user2id} AND f.user2id= #{user1id})")
  end
  
  def as_json(options)
    # this example ignores the user's options
     super(:only => [:email, :first_name, :last_name])
  end

  def name
    first_name+" "+last_name
  end

  def authenticate( pw )
    pw = password
  end
  
  def likes( pic )
    Like.find_by_sql(["
    SELECT likes.* 
    FROM likes 
    WHERE photoid = ? AND userid = ?",pic.id,id]).first
  end

  def self.sort_by_contribution
     
   ActiveRecord::Base.transaction do
     query = User.connection.select_all("
       
       SELECT u.id as cid, COUNT(c.id) as coms 
       INTO UNLOGGED tc FROM users u, comments c
       WHERE u.id = c.userid 
       GROUP BY cid; 
       
       SELECT u.id as pid, COUNT(p.id) as pics 
       INTO UNLOGGED tp FROM users u, albums a, photos p 
       WHERE u.id=a.userid AND p.albumid = a.id 
       GROUP BY pid; 
       
       SELECT * 
       FROM tp FULL JOIN tc ON tp.pid = tc.cid;
     ").map{|row| row.symbolize_keys}
     
      User.connection.select_all("drop table tc,tp;")
      
      res= query.map do |row|
        result = {}
        result[:id] = row[:cid] || row[:pid] 
        result[:user] = User.from_id(result[:id])
        
        result[:score] = 0
        result[:score] +=row[:pics].to_i if row[:pics]
        result[:score] += row[:coms].to_i if row[:coms]
        result
      end
      res.sort_by{|hsh| hsh[:score]*-1}
    end
  end
end
