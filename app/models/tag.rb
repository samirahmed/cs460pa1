class Tag < ActiveRecord::Base
  attr_accessible :word

  has_and_belongs_to_many :photos, :join_table => "tagged" 

  set_primary_key "word"

  def self.from_word(word)
    (Tag.find_by_sql ["
     SELECT tags.*
     FROM tags
     WHERE tags.word = ?",word]).uniq
  end

  def self.find_by_user_id( userid )
      (Tag.find_by_sql ["
        SELECT t.tag_id as word 
        FROM tagged t 
        WHERE t.photo_id in (
          SELECT p.id 
          FROM users u, albums a, photos p 
          WHERE u.id = a.userid 
            AND a.id = p.albumid
            AND u.id = ?)",userid]).compact.uniq
  end
  
  def self.popular
    results = Tag.connection.select_all("
      SELECT tag_id as word, count(photo_id) as total
      FROM tagged 
      GROUP BY tag_id
      ORDER BY total DESC
      LIMIT 25
    ").map{|item| item.symbolize_keys}
      .map{|item| item.merge(total:item[:total].to_i)}
  end

  def self.suggest(words)
    
    word_array = words.map(&:inspect).join(', ').gsub("\"","\'")
    ActiveRecord::Base.transaction do 
      
        suggestions = Tag.find_by_sql(["
        SELECT p.id as pid
        INTO suggest_photos
        FROM photos p
        WHERE EXISTS( SELECT * FROM tagged t WHERE t.tag_id in (#{word_array}) )
          AND NOT EXISTS(
          (
            SELECT t2.tag_id FROM tagged t2
            WHERE t2.tag_id in (#{word_array})
          )
          EXCEPT
          (
            SELECT t3.tag_id FROM tagged t3
            WHERE t3.photo_id = p.id
          )
         );
         SELECT DISTINCT tagged.tag_id as word, COUNT(tagged.photo_id) as total
         FROM suggest_photos JOIN tagged ON suggest_photos.pid = tagged.photo_id
         GROUP BY word
         ORDER BY total DESC
         LIMIT 10;
        "])
        Tag.connection.select_all("drop table suggest_photos")

        suggestions.map{|item|item.word}-words
    end
  end
end
