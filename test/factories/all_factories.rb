require 'factory_girl'

category = ["abstract","animals","business","cats","city","food","nightlife","fashion",
  "people","nature","sports","technics","transport"]

tags = ["photography","camera","scienceman!","helper","faster","unique","better","drink","skys","omg","lol","fashioniable","summer","breezy","fruity","deliciousness","beauty","buttlike","sunny","happy!","greatest!","sunset","beach","cloudy","boring","twitter","sofa","yum","snappy","sassy","sexy","simply","plants","veggie","emo","punk","friendly","swag","yolo","cool","hottt","sweet","lame","lamer","balling","awesome","stevejobs","taglich","windy","rainy","awesomer!","abstract","looser","win!","artistic","dangerous","famous","jealous","green","red","blue","obama","usa","television","internet","technology","funkytown","wow","nice","beautiful","neat","artsy","stunning","scientific","delicious","fast","amazing","great","eye-popping","wowsers"].concat(category)

SENTENCE_URL="http://www.wordgenerator.net/application/p.php?id=sentences_random_statements&type=50"

puts "getting some random sentences"
sentences= 5.times
            .reduce(""){|memo,_| memo+=HTTParty.get(SENTENCE_URL).body}
            .split(".,")

tags = tags.concat("Defiant Slippery Uncovered Aberrant Aberrant Mean Thoughtful Absent Quickest Quickest Smooth Hard-to-find Simplistic Simplistic Whispering Temporary Bitter Bitter Adventurous Scientific Scientific Coordinated Anxious Anxious Coordinated Ruthless Tremendous Overt Unable Unable Ratty Neighborly Ambiguous Ambiguous Fabulous Flaky Shrill Smiling Annoying Uttermost Recondite Dangerous Shiny Dirty Rustic Miscreant Dynamic Attractive Recondite Defiant Defiant Uncovered".split(" ").map{|word| word.downcase}).sort.uniq

FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email     { "#{first_name.downcase}.#{last_name.downcase}@gmail.com"}
    dob       { rand(18..60).years.ago }
    password  "password"
    homecity        {Faker::Address.city}
    homestate       {Faker::AddressUS.state}
    homecountry      "USA"
    currentcity      {Faker::Address.city}
    currentstate     {Faker::AddressUS.state}
    currentcountry   "USA"
    education        {Faker::Education.school}
    gender           {first_name.end_with?('a') ? "Female" : (rand(3)>1? "Male": "Female") }
  end

  factory :album do
    name    { Faker::Address.neighborhood }
    userid  { User.first.id }
    date    { rand(1..300).days.ago }
  end

  factory :photo do
    caption { sentences.sample }
    image   { "http://lorempixel.com/400/400/#{category.sample}/#{rand(1..10)}"}
    albumid { Album.first.id }
  end
  
  factory :comment do
    date    { 2.minutes.ago }
    userid  { User.last.id }
    photoid { Photo.first.id } 
    text    { sentences.sample }
  end

  factory :tag do
    word   { tags.sample }
  end
end
